fn fact(x: i64) -> i64 {
    if x == 1 {
        return 1;
    }

    return x * fact(x - 1);
}

fn main() {
    let b = 5;
    let a = 10;

    println!("{}! = {}", a, fact(a));
    println!("{}! = {}", b, fact(b));
}
